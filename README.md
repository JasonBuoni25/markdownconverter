```

          __  __                    _         _                                      ___                                             _                     
    o O O|  \/  |  __ _      _ _   | |__   __| |    ___   __ __ __ _ _       o O O  / __|    ___    _ _     __ __    ___      _ _   | |_     ___      _ _  
   o     | |\/| | / _` |    | '_|  | / /  / _` |   / _ \  \ V  V /| ' \     o      | (__    / _ \  | ' \    \ V /   / -_)    | '_|  |  _|   / -_)    | '_| 
  TS__[O]|_|__|_| \__,_|   _|_|_   |_\_\  \__,_|   \___/   \_/\_/ |_||_|   TS__[O]  \___|   \___/  |_||_|   _\_/_   \___|   _|_|_   _\__|   \___|   _|_|_  
 {======|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""| {======|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""| 
./o--000'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'./o--000'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-' 

```

# Description

# Usage

#### Markdown
```
usage: markdown.py [-h] [--file FILE] [--string STRING]
                   [--outputfile OUTPUTFILE]

optional arguments:
  -h, --help            show this help message and exit
  --file FILE           File that will be rendered to HTML
  --string STRING       String will be used if no file is sent
  --outputfile OUTPUTFILE
                        Output file location

Examples: 
  python3 markdown.py --file=testfiles/testLarge.txt
  python3 markdown.py --output=test.txt --string="# Sample Document\n\nHello!\n\nThis is sample markdown for the [Mailchimp](https://www.mailchimp.com) homework assignment."
```
#### Markdown Tests
```
usage: markdown_test.py [-h] [-v] [-q] [--locals] [-f] [-c] [-b]
                        [-k TESTNAMEPATTERNS]
                        [tests [tests ...]]

positional arguments:
  tests                a list of any number of test modules, classes and test
                       methods.

optional arguments:
  -h, --help           show this help message and exit
  -v, --verbose        Verbose output
  -q, --quiet          Quiet output
  --locals             Show local variables in tracebacks
  -f, --failfast       Stop on first fail or error
  -c, --catch          Catch Ctrl-C and display results so far
  -b, --buffer         Buffer stdout and stderr during tests
  -k TESTNAMEPATTERNS  Only run tests which match the given substring

Examples:
  markdown_test.py                           - run default set of tests
  markdown_test.py MyTestSuite               - run suite 'MyTestSuite'
  markdown_test.py MyTestCase.testSomething  - run MyTestCase.testSomething
  markdown_test.py MyTestCase                - run all 'test*' test methods
                                       in MyTestCase
```


# Requirements
This script requires Python 3 to run. [Download here!](https://www.python.org/downloads/)
#### Python Libraries used:
- unittest
- re
- argparse


