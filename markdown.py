#!/usr/bin/python
import re
import argparse

"""
    Markdown gives a best guess in most cases. If there are string errors in interpolation, 
    much like HTML we just return the base line and do what we can.
"""


def convert_to_html(text):
    """
    Take HTML and convert it to text. Use the following:

    | Markdown                               | HTML                                              |
    | -------------------------------------- | ------------------------------------------------- |
    | `# Heading 1`                          | `<h1>Heading 1</h1>`                              |
    | `## Heading 2`                         | `<h2>Heading 2</h2>`                              |
    | `...`                                  | `...`                                             |
    | `###### Heading 6`                     | `<h6>Heading 6</h6>`                              |
    | `Unformatted text`                     | `<p>Unformatted text</p>`                         |
    | `[Link text](https://www.example.com)` | `<a href="https://www.example.com">Link text</a>` |
    | `Blank line`                           | `Ignored`                                         |

    :param text: Text as markdown
    :return: markdown as HTML
    """

    html = ''

    if text is None:
        print('ERROR: Invalid text of None.')
        return html

    lines = text.split('\n')
    wrapping = False
    no_wrap = False

    for idx, line in enumerate(lines):
        if line == '':
            built_line, no_wrap = '', True
        else:
            built_line, no_wrap = build_line(line)
        # Wrap only if not first index
        if idx != 0 and not no_wrap:
            if wrapping:
                html = html + f'\n{built_line}'
            else:
                html = html + f'<p>{built_line}'
                wrapping = True
        elif idx == 0 and not no_wrap:
            html = f'<p>{built_line}'
            wrapping = True
        else:
            if wrapping:
                html = html + f'</p>\n{built_line}\n'
                wrapping = False
            else:
                html = html + f'{built_line}\n'

    if not no_wrap:
        html = html + '</p>'
    return html


def build_line(line):
    """
    Read an individual line. We could do all the string building in here, but moving it out
    makes it more unit testable
    :param line:
    :return:
    """
    # Edge cases
    if line == 'Blank line':
        # Not sure if I am supposed to take this literally or not
        return 'Ignored', False
    elif line == '...':
        return line, False
    elif line == '\n':
        return '', True

    try:
        line, header_found = build_header(line)
        line, link_found = build_link_line(line)

        if not header_found and link_found:
            line = f'<p>{line}</p>'

        no_wrap = link_found or header_found
        line = replace_ending_punctuation(line)

        return line, no_wrap
    except Exception as e:
        print(f'Error when parsing {line}: {e}')
        return line, False


def replace_ending_punctuation(line):
    """
    Remove ! and . if at the end of the line
    :return:
    """
    if len(line) == 0:
        return line
    elif line[len(line) - 1] == '!' or line[len(line) - 1] == '.':
        return line[:len(line) - 1]
    else:
        return line


def build_link_line(line):
    """
    Build the link html string
    :param line:
    :return:
    """
    link_regex = re.search("\[(.+)\]\((.+)\)", line)

    # You can have headers in links
    if link_regex:
        span = link_regex.span()
        link = link_regex.group()

        link_href = link[str.index(link, '(') + 1:str.index(link, ')')]
        link_text = link[1:str.index(link, ']')]
        # Edge case if the link is all that is sent
        line_start = '' if span[0] == 0 else line[:span[0]]
        # Edge case if the link is all that is sent
        line_end = '' if span[1] == len(line) else replace_ending_punctuation(line[span[1]:])  # Nested punctuation still goes away
        link_html = f'{line_start}<a href="{link_href}">{link_text}</a>{line_end}'
        return link_html, True
    else:
        return line, False


def build_header(line):
    """
    We know this is a header. Find the number of occurrences of '#' in a row
    :param line:
    :return:
    """
    # This is a little tricky, but we will use this to find the h val
    header_results = re.search("(^#)#*", line)

    if header_results:
        # Will return the length which will be used for the 'h' value
        length = len(header_results.group())
        header_string = line[length:]
        # For header text we typically have 1 space between the header and text
        # If that is the case remove it
        header_string = header_string[1:] if header_string[0] == ' ' else header_string
        header_line = f'<h{length}>{header_string}</h{length}>'
        return header_line, True

    else:
        return line, False


if __name__ == "__main__":
    # This may be over the top, but I am a big fan of argparse, especially when handing off
    # Scripts where command line arguments aren't always well defined
    parser = argparse.ArgumentParser()

    parser.add_argument("--file", help="File that will be rendered to HTML")
    parser.add_argument("--string", help="String will be used if no file is sent")
    parser.add_argument("--outputfile", help="Output file location")
    args = parser.parse_args()

    file_name = args.file
    string = args.string
    output_file = args.outputfile

    html = ''

    if file_name:
        # Read file
        with open(file_name, 'r') as content_file:
            content = content_file.read()
            html = convert_to_html(content)
            print(html)
    elif string:
        html = convert_to_html(string)
        print(html)
    else:
        print('Invalid usage. Must include --file or --string')

    if output_file:
        f = open(output_file, "w")
        f.write(html)
        f.close()


