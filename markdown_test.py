#!/usr/bin/python
# -*- coding: UTF-8-*-
import unittest
from markdown import *

class TestMarkdown(unittest.TestCase):
    def test_parse_header(self):
        self.assertEqual(build_header('# Sample Document'), ('<h1>Sample Document</h1>', True))
        self.assertEqual(build_header('### Sample Document'), ('<h3>Sample Document</h3>', True))
        self.assertEqual(build_header('1234 Sample Document'), ('1234 Sample Document', False))
        self.assertEqual(build_header('###Sample Document'), ('<h3>Sample Document</h3>', True))
        self.assertEqual(build_header('Sample Document'), ('Sample Document', False))
        self.assertEqual(build_header('<h1>Sample Document</h1>'), ('<h1>Sample Document</h1>', False))
        self.assertEqual(build_header(''), ('', False))

    def test_build_link_line(self):
        line, link_found = build_link_line('This is sample markdown for the [Mailchimp](https://www.mailchimp.com) homework assignment.')
        self.assertEqual(line, 'This is sample markdown for the <a href="https://www.mailchimp.com">Mailchimp</a> homework assignment')
        self.assertTrue(link_found)
        line, link_found = build_link_line('[Mailchimp](https://www.mailchimp.com)')
        self.assertEqual(line, '<a href="https://www.mailchimp.com">Mailchimp</a>')
        self.assertTrue(link_found)
        self.assertEqual(build_link_line('(https://www.mailchimp.com)'), ('(https://www.mailchimp.com)', False))
        self.assertEqual(build_link_line('[Mailchimp]'), ('[Mailchimp]', False))
        self.assertEqual(build_link_line('Mailchimp'), ('Mailchimp', False))
        self.assertEqual(build_link_line(''), ('', False))

    def test_replace_ending_punctuation(self):
        self.assertEqual(replace_ending_punctuation('hello.'), 'hello')
        self.assertEqual(replace_ending_punctuation('hello!'), 'hello')
        self.assertEqual(replace_ending_punctuation('hello'), 'hello')
        self.assertEqual(replace_ending_punctuation('hello?'), 'hello?')
        self.assertEqual(replace_ending_punctuation(''), '')
        self.assertEqual(replace_ending_punctuation('?'), '?')
        self.assertEqual(replace_ending_punctuation('.'), '')

    def test_readLine(self):
        line, wrap = build_line('# Sample Document')
        self.assertEqual(line, '<h1>Sample Document</h1>')
        self.assertTrue(wrap)
        line, wrap = build_line('This is sample markdown for the [Mailchimp](https://www.mailchimp.com) homework assignment.')
        self.assertEqual(line, '<p>This is sample markdown for the <a href="https://www.mailchimp.com">Mailchimp</a> homework assignment</p>')
        self.assertTrue(wrap)
        line, wrap = build_line('Hello!')
        self.assertEqual(line, 'Hello')
        self.assertFalse(wrap)
        line, wrap = build_line('...')
        self.assertEqual(line, '...')
        self.assertFalse(wrap)
        line, wrap = build_line('Blank line')
        self.assertEqual(line, 'Ignored')
        self.assertFalse(wrap)
        line, wrap = build_line('## This is a header [with a link](http://yahoo.com)')
        self.assertEqual(line, '<h2>This is a header <a href="http://yahoo.com">with a link</a></h2>')
        self.assertTrue(wrap)

    def test_convert_to_html(self):
        html_to_convert = '# Sample Document\nHello!\nThis is sample markdown for the [Mailchimp](https://www.mailchimp.com) homework assignment.'
        html = convert_to_html(html_to_convert)
        html = html.replace('\n', '')  # Newlines don't matter
        expected_html = '<h1>Sample Document</h1><p>Hello</p><p>This is sample markdown for the <a href="https://www.mailchimp.com">Mailchimp</a> homework assignment</p>'
        self.assertEqual(html, expected_html)

        html_to_convert = '# Header one\nHello there\n\nHow are you?\nWhat\'s going on?\n## Another Header\nThis is a paragraph [with an inline link](http://google.com). Neat, eh?\n## This is a header [with a link](http://yahoo.com)'
        html = convert_to_html(html_to_convert)
        html = html.replace('\n', '')  # Newlines don't matter
        expected_html = '<h1>Header one</h1><p>Hello there</p><p>How are you?What\'s going on?</p><h2>Another Header</h2><p>This is a paragraph <a href="http://google.com">with an inline link</a>. Neat, eh?</p><h2>This is a header <a href="http://yahoo.com">with a link</a></h2>'
        self.assertEqual(html, expected_html)

    def test_convert_to_html_bad(self):
        html_to_convert = 'h# Sample Document\nHello!\nThis is sample markdown for the [Mailchimp]https://www.mailchimp.com) homework assignment.'
        html = convert_to_html(html_to_convert)
        html = html.replace('\n', '')  # Newlines don't matter
        expected_html = '<p>h# Sample DocumentHelloThis is sample markdown for the [Mailchimp]https://www.mailchimp.com) homework assignment</p>'
        self.assertEqual(html, expected_html)
        self.assertEqual(convert_to_html('').replace('\n', ''), '')

        html_to_convert = '# ¿'
        html = convert_to_html(html_to_convert)
        html = html.replace('\n', '')  # Newlines don't matter
        expected_html = '<h1>¿</h1>'
        self.assertEqual(html, expected_html)

        html_to_convert = None
        self.assertEqual(convert_to_html(html_to_convert), '')



if __name__ == '__main__':
    unittest.main()
